main_module.factory('loginFactory', function ($resource) {
    
    var factory = {};
    factory.collectionArray = [];
    
    //This function can be called from ANY controller using this factory implementation
    factory.startLogin = function (data) {
        //console.log('startLogin: ');
        //console.log(data);

        //Save current username to localstorage and sessionstorage. 
        //Used in other controllers to identify current user
        localStorage.username = data.username;
        sessionStorage.username = data.username;
        
        //Create resource for context "/users/login", empty object {} includes options
        var req = $resource('/users/login', {}, {'post': {method: 'POST'}});
        //Use POST method to send the username and password to above context
        //Return an promise object from here
        return req.post(data).$promise;
    };
    
    //Password checking
    factory.checkPass = function (data) {
        var req = $resource('/users/checkPass', {}, {'post': {method: 'POST'}});
        return req.post(data).$promise;
    };
    
    //New user registration
    factory.startRegister = function (data) {
        
        //Create resource for context "/users/register"
        var req = $resource('/users/register', {}, {'post': {method: 'POST'}});
        //Use POST method to send the username and password to above context
        //Return an promise object from here
        return req.post(data).$promise; 
    };
    
    //Updates new data to back end
    factory.insertCollectionData = function (data) {
        var resource = $resource('/users', {}, {'post': {method: 'POST'}});
        return resource.post(data).$promise;
    };
    
    //Factory must always return an object!!!!
    return factory;
    
});
//This is the main controller for all apges
main_module.controller('mainController', function ($scope, mainFactory, $location, Flash, $http, $resource, $filter) {

    var path, addPath, modifyPath, backPath, nameCollectionPath, date, day, month, year, hour, min, sec, today, fintoday, temp, collectionArrayLength, start, end;

    path = location.href.split("#/")[1];
    
    if (path === "horse" || path === "modifyHorse" || path === "addHorse") {
        addPath = "/addHorse";
        modifyPath = "/modifyHorse";
        backPath = "/horse";
        nameCollectionPath = "/horses";
    } else if (path === "feedHorse" || path === "modifyFeedHorse" || path === "addFeedHorse") {
        addPath = "/addFeedHorse";
        modifyPath = "/modifyFeedHorse";
        backPath = "/feedHorse";
        nameCollectionPath = "/horses";
    } else if (path === "healthHorse" || path === "modifyHealthHorse" || path === "addHealthHorse") {
        addPath = "/addHealthHorse";
        modifyPath = "/modifyHealthHorse";
        backPath = "/healthHorse";
        nameCollectionPath = "/horses";
    } else if (path === "horseWelfare" || path === "modifyHorseWelfare" || path === "addHorseWelfare") {
        addPath = "/addHorseWelfare";
        modifyPath = "/modifyHorseWelfare";
        backPath = "/horseWelfare";
        nameCollectionPath = "/horses";
    } else if (path === "horseEquipment" || path === "modifyHorseEquipment" || path === "addHorseEquipment") {
        addPath = "/addHorseEquipment";
        modifyPath = "/modifyHorseEquipment";
        backPath = "/horseEquipment";
        nameCollectionPath = "/horses";
    } else if (path === "horsePasturage" || path === "modifyHorsePasturage" || path === "addHorsePasturage") {
        addPath = "/addHorsePasturage";
        modifyPath = "/modifyHorsePasturage";
        backPath = "/horsePasturage";
        nameCollectionPath = "/horses";
    } else if (path === "horsePasturageGuest" || path === "modifyHorsePasturageGuest" || path === "addHorsePasturageGuest") {
        addPath = "/addHorsePasturageGuest";
        modifyPath = "/modifyHorsePasturageGuest";
        backPath = "/horsePasturageGuest";
        nameCollectionPath = "/horses";
    } else if (path === "person" || path === "modifyPerson" || path === "addPerson") {
        addPath = "/addPerson";
        modifyPath = "/modifyPerson";
        backPath = "/person";
        nameCollectionPath = "/users";
    } else if (path === "contact" || path === "modifyContact" || path === "addContact") {
        addPath = "/addContact";
        modifyPath = "/modifyContact";
        backPath = "/contact";
        nameCollectionPath = "/contacts";
    } else if (path === "todo" || path === "modifyTodo" || path === "addTodo") {
        addPath = "/addTodo";
        modifyPath = "/modifyTodo";
        backPath = "/todo";
        nameCollectionPath = "/users";
    } else if (path === "stableCalendar" || path === "modifyStableCalendar" || path === "addStableCalendar") {
        addPath = "/addStableCalendar";
        modifyPath = "/modifyStableCalendar";
        backPath = "/stableCalendar";
        nameCollectionPath = "/horses";
    } else if (path === "paddoc" || path === "modifyPaddoc" || path === "addPaddoc") {
        addPath = "/addPaddoc";
        modifyPath = "/modifyPaddoc";
        backPath = "/paddoc";
        nameCollectionPath = "/horses";
    } else if (path === "camp" || path === "modifyCamp" || path === "addCamp") {
        addPath = "/addCamp";
        modifyPath = "/modifyCamp";
        backPath = "/camp";
        nameCollectionPath = "/horses"; 
    } else if (path === "campPerson" || path === "modifyCampPerson" || path === "addCampPerson") {
        addPath = "/addCampPerson";
        modifyPath = "/modifyCampPerson";
        backPath = "/campPerson";
        nameCollectionPath = "/camppersons"; 
    } else if (path === "horseHour" || path === "modifyHorseHour" || path === "addHorseHour") {
        addPath = "/addHorseHour";
        modifyPath = "/modifyHorseHour";
        backPath = "/horseHour";
        nameCollectionPath = "/horses";
    } else if (path === "personHour" || path === "modifyPersonHour" || path === "addPersonHour" || path === "personHourList") {
        addPath = "/addPersonHour";
        modifyPath = "/modifyPersonHour";
        backPath = "/personHour";
        nameCollectionPath = "/users";
    } else if (path === "test" || path === "modifyTest" || path === "addTest") {
        addPath = "/addTest";
        modifyPath = "/modifyTest";
        backPath = "/test";
        nameCollectionPath = "/users";
    } else if (path === "main") {
        addPath = "/main";
        modifyPath = "/main";
        backPath = "/main";
        nameCollectionPath = "/users";
    } else if (path === "admin") {
        addPath = "/addUser";
        modifyPath = "/admin";
        backPath = "/admin";
        nameCollectionPath = "/users";
    } else if (path === "campGoogle") {
        addPath = "/campGoogle";
        modifyPath = "/campGoogle";
        backPath = "/campGoogle";
        nameCollectionPath = "/users";
    } else {
        console.log("HTML FILE MISSING!");
    }

    //Set resourse paths
    mainFactory.setFactoryPaths();
    
    //Set default date to today to $scope.Aika, $scope.Paiva, $scope.Now....
    date = new Date();
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
    hour = date.getHours();
    min = date.getMinutes();
    sec = date.getSeconds();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (min < 10) {
        min = "0" + min;
    }
    if (sec < 10) {
        sec = "0" + sec;
    }
    
    today = year + "." + month + "." + day;
    fintoday = day + "." + month + "." + year;
    var monthStart = year + "." + month + "." + day;
    monthNames = ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"];
    $scope.Kuukausi = monthNames[date.getMonth()] + ' ' +  year;
    $scope.Aika = today;
    $scope.kuukausiAlku = year + '.' + month + '.01';
    $scope.Paiva = today;
    $scope.Tyosuhde = today;
    $scope.Now = year + '/' + month + '/' +  day + ' ' + hour + ':' + min;
    var thisMonth = year + "." + month;
    $scope.startTime = $scope.kuukausiAlku + '-';
    $scope.endTime = today;
    
    $scope.personName = localStorage.name;
    $scope.campName = localStorage.campname;

    //Set some variables to default values
    dataDefs = {
        startTime: thisMonth,
        camppi: localStorage.campname,
        hourname: localStorage.hourname
    };
    
    mainFactory.getCollectionData(dataCallback, dataDefs);

    function dataCallback(collectionArray) {
   
        //Set collectionData and document scopes
        $scope.collectionData = collectionArray;
        $scope.document = collectionArray;
        
        if (path === "personHour") {
            //Count total working hours for logged person
            total = 0;
            totalOffice = 0;
            totalCustomer = 0;
            totalRiding = 0;
            totalRunning = 0;
            totalStable = 0;
            totalOther = 0;
            for (var i=0; i < $scope.collectionData.length; i++){
                if ($scope.collectionData[i].Tehtava === "Toimisto") {
                    totalOffice += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Tehtava === "Asiakas") {
                    totalCustomer += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Tehtava === "Ratsutus") {
                    totalRiding += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Tehtava === "Juoksutus") {
                    totalRunning += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Tehtava === "Talli") {
                    totalStable += $scope.collectionData[i].Tunnit;
                } else {
                    totalOther += $scope.collectionData[i].Tunnit;
                }

                total += $scope.collectionData[i].Tunnit;                
            };
            
            $scope.totalHours = total;
            $scope.totalOffice = totalOffice;
            $scope.totalCustomer = totalCustomer;
            $scope.totalRiding = totalRiding;
            $scope.totalRunning = totalRunning;
            $scope.totalStable = totalStable;
            $scope.totalOther = totalOther;
            
        } else if (path === "horseHour") {
            //Count total working hours for horse
            total = 0;
            totalKoulu = 0;
            totalEste = 0;
            totalPuomi = 0;
            totalMaasto = 0;
            totalTalutus = 0;
            totalRatsutus = 0;
            totalJuoksutus = 0;
            totalYleis = 0;
            totalVammais = 0;
            totalSos = 0;
            totalAjo = 0;
            totalMuu = 0;
                  
            for (var i=0; i < $scope.collectionData.length; i++){
                if ($scope.collectionData[i].Laji === "Koulu") {
                    totalKoulu += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Laji === "Este") {
                    totalEste += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Laji === "Puomi") {
                    totalPuomi += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Laji === "Maasto") {
                    totalMaasto += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Laji === "Talutus") {
                    totalTalutus += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Laji === "Ratsutus") {
                    totalRatsutus += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Laji === "Juoksutus") {
                    totalJuoksutus += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Laji === "Yleistunti") {
                    totalYleis += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Laji === "Vammaisratsastus") {
                    totalVammais += $scope.collectionData[i].Tunnit; 
                } else if ($scope.collectionData[i].Laji === "Sos.peda") {
                    totalSos += $scope.collectionData[i].Tunnit;
                } else if ($scope.collectionData[i].Laji === "Ajo") {
                    totalAjo += $scope.collectionData[i].Tunnit;
                } else {
                    totalMuu += $scope.collectionData[i].Tunnit;
                }

                total += $scope.collectionData[i].Tunnit;                
            };

            $scope.totalHours = total;
            $scope.totalKoulu = totalKoulu;
            $scope.totalEste = totalEste;
            $scope.totalPuomi = totalPuomi;
            $scope.totalMaasto = totalMaasto;
            $scope.totalYleis = totalYleis;
            $scope.totalTalutus = totalTalutus;
            $scope.totalRatsutus = totalRatsutus;
            $scope.totalJuoksutus= totalJuoksutus;
            $scope.totalVammais = totalVammais;
            $scope.totalSos = totalSos;
            $scope.totalAjo = totalAjo;
            $scope.totalMuu = totalMuu;

        };
    };

    //HTML Column order selection, default value is alphabetical order
    $scope.reverse = false;
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : true;
        $scope.predicate = predicate;
    };
    
    //Create footer text, create about page for instructions and other general stuff?
    //$(".footer_text").html("Time: " +  $scope.Now + "<br>Copyright by Jarmo Leskinen");
    
    //Initilize datepickers
    $("#datepicker").datepicker({dateFormat: "yy.mm.dd"});
    $("#datepicker2").datepicker({dateFormat: "yy.mm.dd"});
    $("#datepicker3").datepicker({dateFormat: "yy.mm.dd"});
    $("#datepicker4").datepicker({dateFormat: "yy.mm.dd"});
    $("#datepicker5").datepicker({dateFormat: "yy.mm.dd"});
    $("#datepicker6").datepicker({dateFormat: "yy.mm.dd"});

    //This is called when "add" clicked from any on html's using this controller
    //Change path to add*.html
    $scope.add = function () {        
        $location.path(addPath);
    };
    
    //Print current page, print.css used for defitions to noprint areas
    $scope.printPage = function() {
        window.print();
    }
    
    //Collect all horse names, used for html select options
    $scope.checkHorseNames = function () {
        collectionArray = [];
        var resource = $resource("/horses", {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            for (i = 0; i < data.length; i++) {
                collectionArray[i] = data[i].Nimi;
            }
            $scope.horses = collectionArray;
            $scope.Hevonen = collectionArray[0]; 
        });  
    };
    
    //This is called when "addNew" clicked from any on html's using this controller
    //Read and add data to db.
    $scope.addNew = function () {

        if (path === "addHorse") {
            temp = {
                Nimi: $scope.Nimi,
                Kutsumanimi: $scope.Kutsumanimi,
                Syntymaaika: $filter('date')($scope.Syntymaaika, "yyyy.MM.dd"),
                Sukuposti: $scope.Sukuposti
            };
        } else if (path === "addFeedHorse") {
            temp = {
                Hevonen: $scope.Hevonen,
                Aamuruoka: $scope.Aamuruoka,
                Paivaruoka: $scope.Paivaruoka,
                Iltaruoka: $scope.Iltaruoka,
                Huomio: $scope.Huomio
            };
        } else if (path === "addHealthHorse") {
            temp = {
                Hevonen: $scope.Hevonen.trim(),
                Rokotukset: $filter('date')($scope.Rokotukset, "yyyy.MM.dd"),
                Madotukset: $filter('date')($scope.Madotukset, "yyyy.MM.dd"),
                Raspaus: $filter('date')($scope.Raspaus, "yyyy.MM.dd"),
                Laakitys: $scope.Laakitys,
                Yleista: $scope.Yleista
            };
        } else if (path === "addHorseWelfare") {
            temp = {
                Hevonen: $scope.Hevonen.trim(),
                Hieronta: $( "#datepicker" ).datepicker().val(),
                Kengitys: $( "#datepicker2" ).datepicker().val(),
                Yleista: $scope.Yleista
            };
        } else if (path === "addHorseEquipment") {
            temp = {
                Hevonen: $scope.Hevonen.trim(),
                Satula: $scope.Satula,
                Suitset: $scope.Suitset,
                Muut: $scope.Muut
            };
        } else if (path === "addHorsePasturage" || path === "addHorsePasturageGuest" ) {
            temp = {
                Hevonen: $scope.Hevonen.trim(),
                Omistaja: $scope.Omistaja,
                Yhteystiedot: $scope.Yhteystiedot,
                Tulopaiva: $( "#datepicker" ).datepicker().val(),
                Lahtopaiva: $( "#datepicker2" ).datepicker().val(),
                Laskutus: $scope.Laskutus,
                Huomio: $scope.Huomio
            };
        } else if (path === "addPerson") {
            temp = {
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                userclass: $scope.userclass,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet,
                Tyosuhde: $scope.Tyosuhde
            };
        } else if (path === "addContact") {
            temp = {
                Kategoria: $scope.Kategoria,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet
            };
        } else if (path === "addTodo") {
            temp = {
                Aika: $( "#datepicker" ).datepicker().val(),
                Ilmoittaja: $scope.personName,
                Ilmoitus: $scope.Ilmoitus
            };
        } else if (path === "addStableCalendar") {
            temp = {
                Kellonaika: $scope.Kellonaika,
                Maanantai: $scope.Maanantai,
                Tiistai: $scope.Tiistai,
                Keskiviikko: $scope.Keskiviikko,
                Torstai: $scope.Torstai,
                Perjantai: $scope.Perjantai,
                Lauantai: $scope.Lauantai,
                Sunnuntai: $scope.Sunnuntai
            };
        } else if (path === "addStableCalendarHour") {
            temp = {
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Yhteyshenkilo: $scope.Yhteyshenkilo,
                Huomio: $scope.Huomio,
                Tunti: $scope.Tunti
            };
        } else if (path === "addPaddoc") {
            temp = {
                Hevonen: $scope.Hevonen.trim(),
                Tarha: $scope.Tarha,
                Lauma: $scope.Lauma,
                Loimi: $scope.Loimi,
                Huomio: $scope.Huomio
            };
        } else if (path === "addCamp") {
            temp = {
                Leiri: $scope.Leiri,
                Alku: $( "#datepicker" ).datepicker().val(),
                Loppu: $( "#datepicker2" ).datepicker().val(),
                Huomio: $scope.Huomio,
                Osallistujat: $scope.Osallistujat
            };
        } else if (path === "addCampPerson") {
            temp = {
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Yhteyshenkilo: $scope.Yhteyshenkilo,
                Varausmaksu: $scope.Varausmaksu,
                Loppulasku: $scope.Loppulasku,
                Huomio: $scope.Huomio,
                Leiri: localStorage.campname
            };
        } else if (path === "addHorseHour") {
            temp = {
                Aika: $( "#datepicker" ).datepicker().val(),
                Hevonen: $scope.Hevonen.trim(),
                Laji: $scope.Laji,
                Tunnit: $scope.Tunnit,
                Huomio: $scope.Huomio,
                Timestamp: $( "#datepicker" ).datepicker().val().split('.').join("")
            };
        } else if (path === "addPersonHour") {
            temp = {
                Paiva: $( "#datepicker" ).datepicker().val(),
                Ilmoittaja: $scope.personName,
                Tehtava: $scope.Tehtava,
                Tunnit: $scope.Tunnit,
                Timestamp: $( "#datepicker" ).datepicker().val().split('.').join("")
            };
        } else if (path === "addTest") {
            temp = {
                Test1: $scope.Test1,
                Test2: $scope.Test2,
                Aika: $( "#datepicker" ).datepicker().val()
            };
        } else {
            console.log(" DATA MISSING!");
        }
        
        mainFactory.insertCollectionData(temp).then(function (data) {
            //Try to update data to db and create flash info
            mainFactory.collectionArray.push(data.data);
            mainFactory.collectionArray = [];
            $location.path(backPath);
            Flash.create('success', 'Tiedot lisätty tietokantaan!', 'custom-class');
        }, function (error) {

            mainFactory.collectionArray = [];
            $location.path(backPath);
            Flash.create('warning', 'Virhe tietokantaan lisäämisessä!', 'custom-class');
        });
        
        //Send also email to email responsible if option selected (addtodo only)
        if (path === "addTodo" && $scope.checkboxEmail) {
    
            //Read email settings from db
            var resource = $resource("/texts/readEmailAddress", {}, {'get': {method: 'GET'}});
            resource.query().$promise.then(function (data) {
                
                $scope.emailAddress = data[0].Text2;
                $scope.emailPass = data[0].Text3;

                //Collect mailing options and add email subject
                var mailOptions={
                    to : $scope.emailAddress,
                    subject : "Postia Pehtoorilta: " + temp.Ilmoittaja + ", " + temp.Aika,
                    text : temp.Ilmoitus,
                    sender: $scope.emailAddress,
                    senderPass: $scope.emailPass
                }
                
                //Send email and create flash info
                mainFactory.sendEmail(mailOptions).then(function (temp) {

                    Flash.create('success', "Sähköposti lähetetty onnistuneesti.", 'custom-class');
                }, function (error) {

                    Flash.create('warning', 'Sähköpostin lähetys epäonnistui!', 'custom-class');
                });
            });
        };
    };  
    
    //Show only admin rated sections only for admin users
    $scope.adminOnly = function () {
        var resource = $resource("/users/checkClass", {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            $scope.class = data[0].userclass;
            if ($scope.class === "admin") {
                $(".onlyAdmin").show();
                $("td.onlyAdmin").show();
            }
        });
    };
    
    //Store selected id and set modify location path
    $scope.modify = function (id) {
        mainFactory.selected_id = id;
        $location.path(modifyPath).replace();
    };
    
    //Read main page presentation text and set it to ".total_text" section (used on main.html and admin.html pages)
    $scope.readText = function () {
        var resource = $resource("/texts/readtext", {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            $scope.totalText = data[0].Text2;
            //Add text to main page
            $(".total_text").html($scope.totalText);
        });
    };
    
    //Write updated main.html page presentation text back to db
    $scope.writeText = function () {
            
        temp = {
                text: $scope.totalText
                };
        mainFactory.writeText(temp);
        Flash.create('success', "Tekstin muokkaaminen onnistui!", 'custom-class');
    };
    
    //Read email settings from db, used on admin.html if need to updated
    $scope.readEmailAddress = function () {

        var resource = $resource("/texts/readEmailAddress", {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            $scope.emailAddress = data[0].Text2;
            $scope.emailPass = data[0].Text3;
        });
    };
    
    //Write updated emails settings back to db
    $scope.writeEmailAddress = function () {
            
        temp = {
                text2: $scope.emailAddress,
                text3: $scope.emailPass
                };
        mainFactory.writeEmailAddress(temp);
        Flash.create('success', "Sähköpostiasetusten muokkaaminen onnistui!", 'custom-class');
    };
    
    //Go to addUser page, only admin allowed to do this.
    $scope.addUser = function () {
        if ($scope.class === "admin") {  
            $location.path('/addUser');
        } else {
            Flash.create('warning', 'Käyttäjien lisääminen on sallittu vain Admin oikeudella!', 'custom-class');
        }

    };
    
    //This is called when "changePassword" clicked from admin.html.
    //admin check to be reomved from final version, all users can change their own password
    $scope.changePassWord = function () {
            $location.path('/changePassWord');
    };
    
    //Go back from add* and modify* pages
    $scope.backClicked = function () {
        mainFactory.collectionArray = [];
        $location.path(backPath);
    };
    
    //Send email, email program must be available on users PC/tablet/phone..
    $scope.emailClicked = function (email) {
        //Verify that email address is not empty
        if (email === "" || email === undefined || email === null) {
            Flash.create('warning', 'Email osoite puutuu!', 'custom-class');
        } else {
            $('#email_form').attr('action', 'mailto:' + email + '?subject=' + "Viesti Pehtoorilta " + fintoday);
            $('#email_form').submit();
        }
    };
    
    //Open www page to new window
    $scope.wwwClicked = function (wwwPage) {
        //Check if www address is available
        if (wwwPage === "" || wwwPage === undefined || wwwPage === null) {
            
            Flash.create('warning', 'WWW osoite puutuu!', 'custom-class');
        } else {
            
            if (/http/i.test(wwwPage)) {
                window.open(wwwPage);
            } else {
                window.open("http://" + wwwPage);   
            }
        }
    };
    
    //Load campPerson page and store campname to LocalStorage
    $scope.camp = function (camp) {
        $location.path('/campPerson');
        localStorage.campname = camp;
    };
    
    //Load Camp page
    $scope.backToCamp = function () {
        $location.path('/camp');
    };
    
    //Open Google Login Page
    $scope.loginGoogle = function () {
        window.open("https://accounts.google.com/ServiceLoginAuth", 'NewWin', 'toolbar=no,status=no,width=560,height=480');
    };
    
    //Google Logout
    $scope.logoutGoogle = function () {
        window.open("https://accounts.google.com/Logout?service=accountsettings", 'NewWin', 'toolbar=no,status=no,width=560,height=480');
    };
    
    //Pehtoori and Google Logout
    $scope.logOut = function () {
        
        //Google logout
        window.open("https://accounts.google.com/Logout?service=accountsettings", 'NewWin', 'toolbar=no,status=no,width=560,height=480');
        
        //Pehtoori logout and login page load
        var url = "/logout";
        $.get(url, function() {
            location.reload('/');
        });
    };
    
    //Load this data only in modify* pages
    $scope.modifyData = function () {
        //Modify functions Start
        var selectedDocument = mainFactory.getSelectedDocument();
        //Load modify page data only if selected document load was successfull
        if (selectedDocument !== undefined) {
            if (path === "modifyHorse") {
                $scope.id = selectedDocument._id;
                $scope.Nimi = selectedDocument.Nimi;
                $scope.Kutsumanimi = selectedDocument.Kutsumanimi;
                $scope.Syntymaaika = selectedDocument.Syntymaaika;
                $scope.Sukuposti = selectedDocument.Sukuposti;
            } else if (path === "modifyFeedHorse") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Aamuruoka = selectedDocument.Aamuruoka;
                $scope.Paivaruoka = selectedDocument.Paivaruoka;
                $scope.Iltaruoka = selectedDocument.Iltaruoka;
                $scope.Huomio = selectedDocument.Huomio;
            } else if (path === "modifyHealthHorse") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Rokotukset = selectedDocument.Rokotukset;
                $scope.Madotukset = selectedDocument.Madotukset;
                $scope.Raspaus = selectedDocument.Raspaus;
                $scope.Laakitys = selectedDocument.Laakitys;
                $scope.Yleista = selectedDocument.Yleista;
            } else if (path === "modifyHorseWelfare") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Hieronta = selectedDocument.Hieronta;
                $scope.Kengitys = selectedDocument.Kengitys;
                $scope.Yleista = selectedDocument.Yleista;
            } else if (path === "modifyHorseEquipment") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Satula = selectedDocument.Satula;
                $scope.Suitset = selectedDocument.Suitset;
                $scope.Muut = selectedDocument.Muut;
            } else if (path === "modifyHorsePasturage" || path === "modifyHorsePasturageGuest") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Omistaja = selectedDocument.Omistaja;
                $scope.Yhteystiedot = selectedDocument.Yhteystiedot;
                $scope.Tulopaiva = selectedDocument.Tulopaiva;
                $scope.Lahtopaiva = selectedDocument.Lahtopaiva;
                $scope.Laskutus = selectedDocument.Laskutus;
                $scope.Huomio = selectedDocument.Huomio;
            } else if (path === "modifyPerson") {
                $scope.id = selectedDocument._id;
                $scope.Nimi = selectedDocument.Nimi;
                $scope.Osoite = selectedDocument.Osoite;
                $scope.Puhelin = selectedDocument.Puhelin;
                $scope.userclass = selectedDocument.userclass;
                $scope.Sahkoposti = selectedDocument.Sahkoposti;
                $scope.Internet = selectedDocument.Internet;
                $scope.Tyosuhde = selectedDocument.Tyosuhde;
            } else if (path === "modifyContact") {
                $scope.id = selectedDocument._id;
                $scope.Kategoria = selectedDocument.Kategoria;
                $scope.Nimi = selectedDocument.Nimi;
                $scope.Osoite = selectedDocument.Osoite;
                $scope.Puhelin = selectedDocument.Puhelin;
                $scope.Sahkoposti = selectedDocument.Sahkoposti;
                $scope.Internet = selectedDocument.Internet;
            } else if (path === "modifyTodo") {
                $scope.id = selectedDocument._id;
                $scope.Aika = selectedDocument.Aika;
                $scope.Ilmoittaja = selectedDocument.Ilmoittaja;
                $scope.Ilmoitus = selectedDocument.Ilmoitus;
            } else if (path === "modifyStableCalendar") {
                $scope.id = selectedDocument._id;
                $scope.Kellonaika = selectedDocument.Kellonaika;
                $scope.Maanantai = selectedDocument.Maanantai;
                $scope.Tiistai = selectedDocument.Tiistai;
                $scope.Keskiviikko = selectedDocument.Keskiviikko;
                $scope.Torstai = selectedDocument.Torstai;
                $scope.Perjantai = selectedDocument.Perjantai;
                $scope.Lauantai = selectedDocument.Lauantai;
                $scope.Sunnuntai = selectedDocument.Sunnuntai;
            } else if (path === "modifyStableCalendarHour") {
                $scope.id = selectedDocument._id;
                $scope.Nimi = selectedDocument.Nimi;
                $scope.Osoite = selectedDocument.Osoite;
                $scope.Puhelin = selectedDocument.Puhelin;
                $scope.Sahkoposti = selectedDocument.Sahkoposti;
                $scope.Yhteyshenkilo = selectedDocument.Yhteyshenkilo;
                $scope.Huomio = selectedDocument.Huomio;
            } else if (path === "modifyPaddoc") {
                $scope.id = selectedDocument._id;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Tarha = selectedDocument.Tarha;
                $scope.Lauma = selectedDocument.Lauma;
                $scope.Loimi = selectedDocument.Loimi;
                $scope.Huomio = selectedDocument.Huomio;
            } else if (path === "modifyCamp") {
                $scope.id = selectedDocument._id;
                $scope.Leiri = selectedDocument.Leiri;
                $scope.Alku = selectedDocument.Alku;
                $scope.Loppu = selectedDocument.Loppu;
                $scope.Huomio = selectedDocument.Huomio;
                $scope.Osallistujat = selectedDocument.Osallistujat;
             } else if (path === "modifyCampPerson") {
                $scope.id = selectedDocument._id;
                $scope.Nimi = selectedDocument.Nimi;
                $scope.Osoite = selectedDocument.Osoite;
                $scope.Puhelin = selectedDocument.Puhelin;
                $scope.Sahkoposti = selectedDocument.Sahkoposti;
                $scope.Yhteyshenkilo = selectedDocument.Yhteyshenkilo;
                $scope.Varausmaksu = selectedDocument.Varausmaksu;
                $scope.Loppulasku = selectedDocument.Loppulasku;
                $scope.Huomio = selectedDocument.Huomio;
            } else if (path === "modifyHorseHour") {
                $scope.id = selectedDocument._id;
                $scope.Aika = selectedDocument.Aika;
                $scope.Hevonen = selectedDocument.Hevonen;
                $scope.Laji = selectedDocument.Laji;
                $scope.Tunnit = selectedDocument.Tunnit;
                $scope.Huomio = selectedDocument.Huomio;
                $scope.Timestamp = selectedDocument.Timestamp;
            } else if (path === "modifyPersonHour") {
                $scope.id = selectedDocument._id;
                $scope.Paiva = selectedDocument.Paiva;
                $scope.Ilmoittaja = selectedDocument.Ilmoittaja;
                $scope.Tehtava = selectedDocument.Tehtava;
                $scope.Tunnit = selectedDocument.Tunnit;
                $scope.Timestamp = selectedDocument.Timestamp;
            } else if (path === "modifyTest") {
                $scope.id = selectedDocument._id;
                $scope.Test1 = selectedDocument.Test1;
                $scope.Test2 = selectedDocument.Test2;
                $scope.Aika = selectedDocument.Aika;
            }
        } else {
            //Reload previous page if user makes browser page reload on modify phase
            mainFactory.collectionArray = [];
            $location.path(backPath).replace();
        }
    };
    
    //Check current path and do updates
    $scope.update = function () {
        if (path === "modifyHorse") {
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Kutsumanimi: $scope.Kutsumanimi,
                Syntymaaika: $scope.Syntymaaika,
                Sukuposti: $scope.Sukuposti
            };
        } else if (path === "modifyFeedHorse") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Aamuruoka: $scope.Aamuruoka,
                Paivaruoka: $scope.Paivaruoka,
                Iltaruoka: $scope.Iltaruoka,
                Huomio: $scope.Huomio
            };
        } else if (path === "modifyHealthHorse") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Raspaus: $scope.Raspaus,
                Madotukset: $scope.Madotukset,
                Rokotukset: $scope.Rokotukset,
                Laakitys: $scope.Laakitys,
                Yleista: $scope.Yleista
            };
        } else if (path === "modifyHorseWelfare") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Hieronta: $( "#datepicker" ).datepicker().val(),
                Kengitys: $( "#datepicker2" ).datepicker().val(),
                Yleista: $scope.Yleista
            };
        } else if (path === "modifyHorseEquipment") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Satula: $scope.Satula,
                Suitset: $scope.Suitset,
                Muut: $scope.Muut
            };
        } else if (path === "modifyHorsePasturage" || path === "modifyHorsePasturageGuest") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Omistaja: $scope.Omistaja,
                Yhteystiedot: $scope.Yhteystiedot,
                Tulopaiva:$( "#datepicker" ).datepicker().val(),
                Lahtopaiva: $( "#datepicker2" ).datepicker().val(),
                Laskutus: $scope.Laskutus,
                Huomio: $scope.Huomio
            };
        } else if (path === "modifyPerson") {
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                userclass: $scope.userclass,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet,
                Tyosuhde: $( "#datepicker" ).datepicker().val()
            };
        } else if (path === "modifyContact") {
            temp = {
                id: $scope.id,
                Kategoria: $scope.Kategoria,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet
            };
        } else if (path === "modifyTodo") {
            temp = {
                id: $scope.id,
                Aika: $( "#datepicker" ).datepicker().val(),
                Ilmoittaja: $scope.Ilmoittaja,
                Ilmoitus: $scope.Ilmoitus
            };
        } else if (path === "modifyStableCalendar") {
            temp = {
                id: $scope.id,
                Kellonaika: $scope.Kellonaika,
                Maanantai: $scope.Maanantai,
                Tiistai: $scope.Tiistai,
                Keskiviikko: $scope.Keskiviikko,
                Torstai: $scope.Torstai,
                Perjantai: $scope.Perjantai,
                Lauantai: $scope.Lauantai,
                Sunnuntai: $scope.Sunnuntai
            };
        } else if (path === "modifyStableCalendarHour") {
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Yhteyshenkilo: $scope.Yhteyshenkilo,
                Huomio: $scope.Huomio,
            };
        } else if (path === "modifyPaddoc") {
            temp = {
                id: $scope.id,
                Hevonen: $scope.Hevonen,
                Tarha: $scope.Tarha,
                Lauma: $scope.Lauma,
                Loimi: $scope.Loimi,
                Huomio: $scope.Huomio
            };
        } else if (path === "modifyCamp") {
            temp = {
                id: $scope.id,
                Leiri: $scope.Leiri,
                Alku: $( "#datepicker" ).datepicker().val(),
                Loppu:$( "#datepicker2" ).datepicker().val(),
                Huomio: $scope.Huomio,
                Osallistujat: $scope.Osallistujat
            };
        } else if (path === "modifyCampPerson") {
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Yhteyshenkilo: $scope.Yhteyshenkilo,
                Varausmaksu: $scope.Varausmaksu,
                Loppulasku: $scope.Loppulasku,
                Huomio: $scope.Huomio,
            };
        } else if (path === "modifyHorseHour") {
            temp = {
                id: $scope.id,
                Aika: $( "#datepicker" ).datepicker().val(),
                Hevonen: $scope.Hevonen,
                Laji: $scope.Laji,
                Tunnit: $scope.Tunnit,
                Huomio: $scope.Huomio,
                Timestamp: $( "#datepicker" ).datepicker().val().split('.').join("")
            };
        } else if (path === "modifyPersonHour") {
            temp = {
                id: $scope.id,
                Paiva: $( "#datepicker" ).datepicker().val(),
                Ilmoittaja: $scope.Ilmoittaja,
                Tehtava: $scope.Tehtava,
                Tunnit: $scope.Tunnit,
                Timestamp: $( "#datepicker" ).datepicker().val().split('.').join("")
            };
        } else if (path === "modifyTest") {
            temp = {
                id: $scope.id,
                Test1: $scope.Test1,
                Test2: $scope.Test2,
                Aika: $( "#datepicker" ).datepicker().val()
            };
        } else if (path === "changePassWord") {
            temp = {
                id: $scope.id,
                password: $scope.passNew1
            };
        }
        
        mainFactory.updateData(temp).then(success, error);
    };
    
    function success() {
        mainFactory.collectionArray = [];
        $location.path(backPath).replace();
        Flash.create('success', 'Tiedot päivitetty onnistuneesti!', 'custom-class');
    }
    
    function error(data) {
        Flash.create('warning', 'Tietojen päivitys epäonnistui, yritä uudelleen!', 'custom-class');
    }
    
    //This is called when updatePerson Clicked
    $scope.updatePerson = function () {
        //Check that user does not remove own information because it may cause empty userbase
        if ($scope.Nimi === localStorage.name) {
            
            //Can't change own userclass (must be admin if here)
            //Because need to avoid admin user remove admin rights from themselves
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet,
                userclass: "admin",
                Tyosuhde: $( "#datepicker" ).datepicker().val()
            };
            mainFactory.updateData(temp).then(success, error);
        } else {
            temp = {
                id: $scope.id,
                Nimi: $scope.Nimi,
                Osoite: $scope.Osoite,
                Puhelin: $scope.Puhelin,
                userclass: $scope.userclass,
                Sahkoposti: $scope.Sahkoposti,
                Internet: $scope.Internet,
                Tyosuhde: $( "#datepicker" ).datepicker().val()
            };
            mainFactory.updateData(temp).then(success, error);
        };
    };
        
            
    //This is called when deletePerson Clicked
    $scope.deletePerson = function () {
        //Check that user does not remove own information because it may cause empty userbase
        if ($scope.Nimi === localStorage.name) {

            mainFactory.collectionArray = [];
            $location.path(backPath).replace();
            Flash.create('warning', 'Et voi poistaa omia tietojasi!', 'custom-class');
        } else {
                //Check that user really knows what she/he is doing
                $( "#dialog-delete" ).dialog({
                    width: 'auto',
                    height: 'auto',
                    modal: true,
                    fluid: true, 
                    resizable: false,
                    show: { effect: "explode" },
                    buttons: {
                    "Kyllä haluan": function() {
                        executeDelete();
                    },
                    "En halua": function() {
                        cancelSearch();
                    }
                  }
                });
                $("#dialog-delete").dialog("open");
        }
    };
 
    
    //Modify Funtions End
    
    //This is called when add allHourSearch clicked from any on html's using this controller
    $scope.allHourSearch = function () {
            $("#dialog-form").dialog({
                width: 450,
                height: 'auto',
                modal: true,
                fluid: true, 
                resizable: false,
                show: { effect: "blind", duration: 300 },
                buttons: {
                "Suorita Haku": function() {
                    start = $( "#datepicker" ).datepicker().val();
                    end = $( "#datepicker2" ).datepicker().val();
                    $scope.personName = "";
                    $scope.horseName = "";
                    $scope.startTime = start + '-';
                    $scope.endTime = end;
                    temp = {
                        startTime: start,
                        endTime: end
                    };
                    executeSearchAll(temp);
                    },

                Peruuta: function() {
                    cancelSearch(); 
                }
            }});
            $("#dialog-form").dialog("open");
    };
    
    //This is called when timeSearch clicked from any on html's using this controller
    //Search by start date <-> end date and name
    $scope.timeSearch = function () {    
        $("#dialog-form").dialog({
            width: 450,
            height: 'auto',
            modal: true,
            fluid: true, 
            resizable: false,
            show: { effect: "blind", duration: 300 },
            onSelect : function () {
    	$("#dialog-form").focus();
    },
            buttons: {
            "Suorita Haku": function() {
                start = $( "#datepicker" ).datepicker().val();
                end = $( "#datepicker2" ).datepicker().val();
                $scope.personName = localStorage.name;
                $scope.horseName = "";
                $scope.startTime = start + '-';
                $scope.endTime = end;
                temp = {
                        startTime: start,
                        endTime: end,
                        name: localStorage.name
                    }

                executeTimeSearch(temp);
                },

            Peruuta: function() {
                cancelSearch(); 
            }
        }});

        $("#dialog-form").dialog("open");
    };
    
    //This is called when searchHorse clicked from any on html's using this controller
    //Search by start date <-> end date and name
    $scope.searchHorse = function(){
        collectionArray = [];
        var resource = $resource(nameCollectionPath, {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            for (i = 0; i < data.length; i++) {
                collectionArray[i] = data[i].Nimi;
            }
        
            $scope.searchHorses = collectionArray;   
        
            $("#dialog-form-name").dialog({
                width: 450,
                height: 'auto',
                modal: true,
                fluid: true, 
                resizable: false,
                show: { effect: "blind", duration: 300 },
                buttons: {
                "Suorita Haku": function() {
                start = $( "#datepicker3" ).datepicker().val();
                end = $( "#datepicker4" ).datepicker().val();
                searchName = $( "#namepickerHorse" ).val();
                $scope.horseName = searchName;
                $scope.startTime = start + '-';
                $scope.endTime = end;
                temp = {
                    startTime: start,
                    endTime: end,
                    item: searchName
                };
                    executeSearch(temp);
                },

                Peruuta: function() {
                    cancelSearch(); 
                }
            }});

            $("#dialog-form-name").dialog("open");
            
        });
    };
    
    //This is called when searchPerson clicked from any on html's using this controller
    //Search by start date <-> end date and name
    $scope.searchPerson = function(){
        collectionArray = [];
        var resource = $resource(nameCollectionPath, {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            for (i = 0; i < data.length; i++) {
                collectionArray[i] = data[i].Nimi;
            }
        
            $scope.searchPersons = collectionArray; 
        
            $("#dialog-form-name-person").dialog({
                width: 450,
                height: 'auto',
                modal: true,
                fluid: true, 
                resizable: false,
                show: { effect: "blind", duration: 300 },
                buttons: {
                "Suorita Haku": function() {
                start = $( "#datepicker5" ).datepicker().val();
                end = $( "#datepicker6" ).datepicker().val();
                searchName = $( "#namepickerPerson" ).val();
                $scope.personName = searchName;
                $scope.startTime = start + '-';
                $scope.endTime = end;
                temp = {
                    startTime: start,
                    endTime: end,
                    item: searchName
                };
                executeSearch(temp);
                },

                Peruuta: function() {
                    cancelSearch(); 
                }
            }});

            $("#dialog-form-name-person").dialog("open");
        }); 
    };
    
    //This is called when resetting previous search, reload current page
    $scope.showAll = function () {
        location.reload();
    };
    
    //executeSearch and close dialog window
    function executeSearch(temp) {
        mainFactory.searchItem(dataCallback, temp);
        $("#dialog-form").dialog("close");
        $("#dialog-form-name").dialog("close");
        $("#dialog-form-name-person").dialog("close");
    };
    
    //executeTimeSearch and close dialog window
    function executeTimeSearch(temp) {
        mainFactory.collectionArray = [];
        mainFactory.getCollectionData(dataCallback, temp);
        $("#dialog-form").dialog("close");
        $("#dialog-form-name").dialog("close");
        $("#dialog-form-name-person").dialog("close");
    };
    
    //executeSearchAll and close dialog window
    function executeSearchAll(temp) {
        mainFactory.collectionArray = [];
        mainFactory.getCollectionDataAll(dataCallback, temp);
        $("#dialog-form").dialog("close");
        $("#dialog-form-name").dialog("close");
        $("#dialog-form-name-person").dialog("close");
    };
    
    //cancelSearch and close dialog window
    function cancelSearch() {
        $("#dialog-form").dialog("close");
        $("#dialog-form-name").dialog("close");
        $("#dialog-form-name-person").dialog("close");
        $("#dialog-confirm").dialog("close");
        $("#dialog-delete").dialog("close");
    }; 
    
    //Init dialog windows and set autoOpen to flase (this prevent window open when html page is loaded)
    $scope.searchInit = function () {

        dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            onSelect : function () {
                $("#dialog-form").focus();
            }
        });

        dialogName = $( "#dialog-form-name" ).dialog({
              autoOpen: false,
        });
        
        dialogPerson = $( "#dialog-form-name-person" ).dialog({
            autoOpen: false,
        });
        
        dialogConfirm = $( "#dialog-confirm" ).dialog({
            autoOpen: false,
        });
        
        dialogDelete = $( "#dialog-delete" ).dialog({
            autoOpen: false,
        });
        
    }
    
    //back funtions, close dialog window and reload previous page
    $scope.back = function () {
        $( "#dialog-confirm" ).dialog({
            width: 'auto',
            height: 'auto',
            modal: true,
            fluid: true, 
            resizable: false,
            show: { effect: "explode" },
            buttons: {
            "Kyllä haluan": function() {
                $("#dialog-confirm").dialog("close");

                if (/modify/i.test(path)) {
                    location.reload();
                } else {
                    history.go(-1);  
                }
            },
            "En halua": function() {
                cancelSearch();
            }
          }
        });
        $("#dialog-confirm").dialog("open");
    };
    
    //Verify that user really want to do delete and execute delete if required
    $scope.delete = function () {
        $( "#dialog-delete" ).dialog({
            width: 'auto',
            height: 'auto',
            modal: true,
            fluid: true, 
            resizable: false,
            show: { effect: "explode" },
            buttons: {
                "Kyllä haluan": function() {
                    executeDelete();
                },
                "En halua": function() {
                    cancelSearch();
                }
            }
        });
        $("#dialog-delete").dialog("open");
    };
    
    //Execute data delete, initialize delete id's (id, Leiri or Hevonen)
    function executeDelete() {

        temp = {
            id: $scope.id,
            Leiri: localStorage.campname,
            Hevonen: $scope.Hevonen
        };

        mainFactory.deleteData(temp).then(function (temp) {
            Flash.create('success', "Tiedot poistettu tietokannasta!", 'custom-class');
            mainFactory.collectionArray = [];
            $location.path(backPath).replace();

        }, function (error) {

            Flash.create('warning', 'Error in server!', 'custom-class');
        });
        $("#dialog-delete").dialog("close");
    };
    
    
    //NodeMailer sendEmail
    
    $scope.sendEmail = function()  {
        
        //Read email settings from db
        var resource = $resource("/texts/readEmailAddress", {}, {'get': {method: 'GET'}});
        resource.query().$promise.then(function (data) {
            $scope.emailAddress = data[0].Text2;
            $scope.emailPass = data[0].Text3;
            
            //Collect mailing options
            var mailOptions={
                to : $scope.to,
                subject : $scope.subject,
                text : $scope.text,
                sender: $scope.emailAddress,
                senderPass: $scope.emailPass
            }
            //Show sending info
            $(".sending").html("<button class='btn btn-primary'><span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>..Lähettää...</button>");
            
            //Send email
            mainFactory.sendEmail(mailOptions).then(function (temp) {

                Flash.create('success', "Sähköposti lähetetty onnistuneesti.", 'custom-class');
                $(".sending").html("");
            }, function (error) {

                Flash.create('warning', 'Sähköpostin lähetys epäonnistui!', 'custom-class');
                $(".sending").html("");
            });
        });
    };
    
    $scope.btnExport = function () {
        
        window.open('data:application/vnd.ms-excel,' + escape($('#dvData0').html()) + escape($('#dvData').html()));
        //e.preventDefault();
    };
    
});
//This is the controller for login and register
main_module.controller('loginController', function ($scope, loginFactory, $location, Flash, $http, $resource) {
    
    $scope.username = localStorage.username;
    $("#datepicker").datepicker({dateFormat: "yy.mm.dd"});
    
    //This is called when login button is pressed in login.html
    $scope.loginClicked = function () {
        
        if (!cookieEnabled) {
            Flash.create('danger', 'Asetuksesi eivät salli Pehtoorin asettaa evästeitä ja siten estävät ohjelman käytön. Katso ohjeet evästeiden (cookies) sallimiseksi selaimellasi.');
        }
        
        var temp = {
            username: $scope.user,
            password: $scope.pass
        };
        
        //wait the response from server
        var waitPromise = loginFactory.startLogin(temp);
        waitPromise.then(function succ(data) {
            //Code inside this block will be called when success response from server receives
            localStorage.name = data.name;
            sessionStorage['token'] = data.secret;
            $location.path('/main');
            
        }, function err(data) {
            //Code inside this block will be called if error response
            Flash.create('danger', 'Wrong username or password! Please try again.', 'custom-class');
        });
    };
    
    $scope.registerClicked = function () {
        
        var temp = {
            username: $scope.user,
            password: $scope.pass,
            userclass: $scope.userclass,
            Nimi: $scope.Nimi,
            Osoite: $scope.Osoite,
            Puhelin: $scope.Puhelin,
            Sahkoposti: $scope.Sahkoposti,
            Tyosuhde: $( "#datepicker" ).datepicker().val()
        };

        //wait the response from server
        var waitPromise = loginFactory.startRegister(temp);
        waitPromise.then(function succ(data) {
            //Code inside this block will be called when success response
            //console.log('Register status was ok');
            Flash.create('success', 'New user registrated', 'custom-class');
            
        }, function err(data) {
            //Code inside this block will be called if error response
            //console.log('Register Error:' + data.status);
            Flash.create('danger', 'Username ' + $scope.user + ' is already in use! Please use some other username!', 'custom-class');
        });
        
        //Back to admin page
        $location.path('/admin').replace();
          
    };
    
    //This is called when changePassClicked button clicked from changePassWord.html
    $scope.changePassClicked = function () {

        if ($scope.passNew1 === undefined || $scope.passNew2 === undefined || $scope.passOld === undefined) {
            Flash.create('warning', 'Salasanat ei ole kirjoitettu oikein, yritä uudelleen!', 'custom-class');
        } else {

            var passLength = $scope.passNew1.length;
            if ($scope.passNew1 === $scope.passNew2 && $scope.passNew1 !== $scope.passOld) {
                var temp = {
                    username: localStorage.username,
                    password: $scope.passOld
                };
                var waitPromise = loginFactory.checkPass(temp);
                //wait the response from server
                waitPromise.then(function (data) {
                    newPass = {
                        username: localStorage.username,
                        password: $scope.passNew1
                    };

                    var temp = {
                        oldpassword: $scope.passOld,
                        newpassword: $scope.passNew1,
                        username: localStorage.username
                    };

                    $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
                    var resource = $resource('/users/changePass', {}, {'put': {method: 'PUT'}});

                    resource.put(temp).$promise.then(function (data) {
                        Flash.create('success', 'Salasanan muuttaminen onnistui!', 'custom-class');
                        $location.path('/admin').replace();
                    }, function (error) {
                        Flash.create('danger', 'Salasanan muuttaminen epäonnistui, yritä uudelleen!', 'custom-class');
                    });

                }, function (error) {
                    //Code inside this block will be called when error response
                    Flash.create('danger', 'Vanha salasana ei ole oikein, yritä uudelleen!', 'custom-class');
                });

            } else {
                Flash.create('warning', 'Salasanat ei ole kirjoitettu oikein, yritä uudelleen!', 'custom-class');
            }
        }
    };
    
    $scope.back = function () {
    $( "#dialog-confirm" ).dialog({
        width: 'auto',
        height: 'auto',
        modal: true,
        fluid: true, 
        resizable: false,
        show: { effect: "explode" },
        buttons: {
        "Kyllä haluan": function() {
            $("#dialog-confirm").dialog("close");
            history.go(-1);  
        },
        "En halua": function() {
            $("#dialog-confirm").dialog("close");
        }
      }
    });
        $("#dialog-confirm").dialog("open");
  };
    
    $scope.backInit = function () {
        dialogBack = $( "#dialog-confirm" ).dialog({
            autoOpen: false,
        });
    }
    
    $scope.default = "basic";
 
    $scope.userClasses =
        [
            "admin",
            "basic"
        ];

    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
        document.cookie = "testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") !== -1) ? true : false;
    }
    
    /*return (cookieEnabled);*/
    if (!cookieEnabled) {
        Flash.create('danger', 'Asetuksesi eivät salli Pehtoorin asettaa evästeitä ja siten estävät ohjelman käytön. Katso ohjeet evästeiden (cookies) sallimiseksi selaimellasi.', 'custom-class');
    }
});
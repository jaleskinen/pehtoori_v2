"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /pasturage context
router.get('/', function (req, res) {
    
    db.getAllPasturages(req, res);
});

//Handle POST request for /pasturage context
router.post('/', function (req, res) {
    
    db.saveNewPasturage(req, res);
});

//Handle UPDATE request for /pasturage context
router.put('/', function (req, res) {
    
    db.updatePasturage(req, res);
});

//Handle DELETE request for /pasturage context
router.delete('/', function (req, res) {
    
    db.deletePasturage(req, res);
});

module.exports = router;
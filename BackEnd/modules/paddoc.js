"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /Paddoc.context
router.get('/', function (req, res) {
    
    db.getAllPaddocs(req, res);
});

//Handle POST request for /Paddoc.context
router.post('/', function (req, res) {
    
    db.saveNewPaddoc(req, res);
});

//Handle UPDATE request for /Paddoc.context
router.put('/', function (req, res) {
    
    db.updatePaddoc(req, res);
});

//Handle DELETE request for /Paddoc.context, id comes from request
router.delete('/', function (req, res) {
    
    db.deletePaddoc(req, res);
});

module.exports = router;
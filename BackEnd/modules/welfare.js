"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /welfare context
router.get('/', function (req, res) {
    
    db.getAllWelfares(req, res);
});

//Handle POST request for /welfare context
router.post('/', function (req, res) {
    
    db.saveNewWelfare(req, res);
});

//Handle UPDATE request for /welfare context
router.put('/', function (req, res) {
    
    db.updateWelfare(req, res);
});

//Handle DELETE request for /welfare context
router.delete('/', function (req, res) {
    
    db.deleteWelfare(req, res);
});

module.exports = router;
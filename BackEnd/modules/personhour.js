"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /Personhour.context
router.get('/', function (req, res) {
    db.getUserPersonhours(req, res);
});

//Handle GET request for /Personhour.context
router.get('/all', function (req, res) {
    
    db.getAllPersonhours(req, res);
});

//Handle POST request for /Personhour.context
router.post('/', function (req, res) {
    
    db.saveNewPersonhour(req, res);
});

//Handle UPDATE request for /Personhour.context
router.put('/', function (req, res) {
    
    db.updatePersonhour(req, res);
});

//Handle DELETE request for /Personhour.context, id comes from request
router.delete('/', function (req, res) {
    
    db.deletePersonhour(req, res);
});

//Handle search request for /Personhour context
router.get('/search', function (req, res) {
    db.findPersonHours(req, res);
});

module.exports = router;
"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /campperson context
router.get('/', function (req, res) {
    
    db.getAllCampPersons(req, res);
});

//Handle POST request for /campperson context
router.post('/', function (req, res) {
    
    db.saveNewCampPerson(req, res);
});

//Handle UPDATE request for /campperson context
router.put('/', function (req, res) {
    
    db.updateCampPerson(req, res);
});

//Handle DELETE request for /campperson context
router.delete('/', function (req, res) {
    
    db.deleteCampPerson(req, res);
});

module.exports = router;
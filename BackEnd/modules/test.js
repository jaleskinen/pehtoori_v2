"use strict";
var express = require("express");
var db = require('./queries');
var nodemailer = require('nodemailer');

var router = express.Router();

//Handle GET request for /test context
router.get('/', function (req, res) {
    
    db.getAllTests(req, res);
});

//Handle POST request for /test context
router.post('/', function (req, res) {
    db.saveNewTest(req, res);
});

//Handle UPDATE request for /test context
router.put('/', function (req, res) {
    
    db.updateTest(req, res);
});

//Handle DELETE request for /test context, id comes from request
router.delete('/', function (req, res) {
    
    db.deleteTest(req, res);
});

//Handle search request for /test context
router.get('/search', function (req, res) {
    db.findTestItems(req, res);
});

//Handle search request for /test context
router.post('/sendEmail', function (req, res) {
    db.sendEmail(req, res);
});

////Handle search request for /test context
//router.get('/readtext', function (req, res) {
//    db.readText(req, res);
//});
//
////Handle search request for /test context
//router.put('/writetext', function (req, res) {
//    db.writeText(req, res);
//});

module.exports = router;
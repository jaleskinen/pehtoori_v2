"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /equipment context
router.get('/', function (req, res) {
    
    db.getAllEquipments(req, res);
});

//Handle POST request for /equipment context
router.post('/', function (req, res) {
    
    db.saveNewEquipment(req, res);
});

//Handle UPDATE request for /equipment context
router.put('/', function (req, res) {
    
    db.updateEquipment(req, res);
});

//Handle DELETE request for /equipment context
router.delete('/', function (req, res) {
    
    db.deleteEquipment(req, res);
});

module.exports = router;
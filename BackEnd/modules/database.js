var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var db_name = 'pehtoori_Leskinen';
var mongodb_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;

// if OPENSHIFT env variables are present, use the available connection info:
if (process.env.OPENSHIFT_MONGODB_DB_URL) {
    mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL +
    process.env.OPENSHIFT_APP_NAME;
}

mongoose.connect(mongodb_connection_string,connectionStatus);

/* Connetion callback for fail and ok cases */
function connectionStatus(err, ok) {
    
    if (err) {
        
        console.log("We are NOT conneted:");
        console.log(err.message);
    } else {
        
        console.log("We are conneted:");
    }
}

var Person = new mongoose.Schema({
    Nimi: String,
    Osoite: String,
    Kategoria: String,
    Puhelin: String,
    Sahkoposti: String,
    Internet: String,
    Tyosuhde: String,
    username: {type: String, unique: true},
    password: String,
    userclass: String,
    personhours:[{type:mongoose.Schema.Types.ObjectId,ref:'Personhour'}]
});

//passport
Person.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

Person.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var person = mongoose.model('Person', Person, 'persons');

//Using exports object you expose the data to other modules
exports.Person = person;

var Contact = mongoose.model('Contact', {
    Kategoria: String,
    Nimi: String,
    Osoite: String,
    Puhelin: String,
    Sahkoposti: String,
    Internet: String
}, 'contact');

//Using exports object you expose the data to other modules
exports.Contact = Contact;

var Horse = mongoose.model('Horse', {
    Nimi: String,
    Kutsumanimi: String,
    Syntymaaika: String,
    Sukuposti: String,
    horsehours:[{type:mongoose.Schema.Types.ObjectId,ref:'Horsehour'}]
}, 'horse');

//Using exports object you expose the data to other modules
exports.Horse = Horse;

var Horsehour = mongoose.model('Horsehour', {
    Aika: String,
    Hevonen: String,
    Laji: String,
    Tunnit: Number,
    Huomio: String,
    Timestamp: String
}, 'horsehour');

//Using exports object you expose the data to other modules
exports.Horsehour = Horsehour;

var Todo = mongoose.model('Todo', {
    Aika: String,
    Ilmoittaja: String,
    Ilmoitus: String
}, 'todo');

//Using exports object you expose the data to other modules
exports.Todo = Todo;

var Personhour = mongoose.model('Personhour', {
    Paiva: String,
    Ilmoittaja: String,
    Tehtava: String,
    Tunnit: Number,
    Timestamp: String
}, 'personhour');

//Using exports object you expose the data to other modules
exports.Personhour = Personhour;

var Stablecalendar = mongoose.model('Stablecalendar', {
    Kellonaika: String,
    Maanantai: String,
    Tiistai: String,
    Keskiviikko: String,
    Torstai: String,
    Perjantai: String,
    Lauantai: String,
    Sunnuntai: String,
    hours:[{type:mongoose.Schema.Types.ObjectId,ref:'StablecalendarHour'}]
}, 'stablecalendar');

//Using exports object you expose the data to other modules
exports.Stablecalendar = Stablecalendar;

var Paddoc = mongoose.model('Paddoc', {
    Hevonen: String,
    Tarha: String,
    Lauma: String,
    Loimi: String,
    Huomio: String
}, 'paddoc');

//Using exports object you expose the data to other modules
exports.Paddoc = Paddoc;

var Feed = mongoose.model('Feed', {
    Hevonen: String,
    Aamuruoka: String,
    Paivaruoka: String,
    Iltaruoka: String,
    Huomio: String
}, 'feed');

//Using exports object you expose the data to other modules
exports.Feed = Feed;

var Health = mongoose.model('Health', {
    Hevonen: String,
    Rokotukset: String,
    Madotukset: String,
    Raspaus: String,
    Laakitys: String,
    Yleista: String
}, 'health');

//Using exports object you expose the data to other modules
exports.Health = Health;

var Welfare = mongoose.model('Welfare', {
    Hevonen: String,
    Hieronta: String,
    Kengitys: String,
    Yleista: String
}, 'welfare');

//Using exports object you expose the data to other modules
exports.Welfare = Welfare;

var Equipment = mongoose.model('Equipment', {
    Hevonen: String,
    Satula: String,
    Suitset: String,
    Muut: String
}, 'equipment');

//Using exports object you expose the data to other modules
exports.Equipment = Equipment;

var Pasturage = mongoose.model('Pasturage', {
    Hevonen: String,
    Omistaja: String,
    Yhteystiedot: String,
    Tulopaiva: String,
    Lahtopaiva: String,
    Laskutus: String,
    Huomio: String
}, 'pasturage');

//Using exports object you expose the data to other modules
exports.Pasturage = Pasturage;

var PasturageGuest = mongoose.model('PasturageGuest', {
    Hevonen: String,
    Omistaja: String,
    Yhteystiedot: String,
    Tulopaiva: String,
    Lahtopaiva: String,
    Laskutus: String,
    Huomio: String
}, 'pasturageGuest');

//Using exports object you expose the data to other modules
exports.PasturageGuest = PasturageGuest;

var Camp = mongoose.model('Camp', {
    Leiri: String,
    Alku: String,
    Loppu: String,
    Huomio: String,
    Osallistujat: String,
    persons:[{type:mongoose.Schema.Types.ObjectId,ref:'Campperson'}]
}, 'camp');

//Using exports object you expose the data to other modules
exports.Camp = Camp;

var Campperson = mongoose.model('Campperson', {
    Nimi: String,
    Osoite: String,
    Puhelin: String,
    Sahkoposti: String,
    Yhteyshenkilo: String,
    Varausmaksu: String,
    Loppulasku: String,
    Huomio: String
}, 'campperson');

//Using exports object you expose the data to other modules
exports.Campperson = Campperson;

var Text = mongoose.model('Text', {
    Text1: String,
    Text2: String,
    Text3: String
}, 'text');

//Using exports object you expose the data to other modules
exports.Text = Text;


var Test = mongoose.model('Test', {
    Test1: String,
    Test2: String,
    Aika: String
}, 'test');

//Using exports object you expose the data to other modules
exports.Test = Test;

exports.myFunction = function () {
    
    console.log("This ");

};


 



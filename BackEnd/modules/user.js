"use strict";
var query = require('./queries');
var express = require("express");

//passport
var passport = require('../../server').passport;

var router = express.Router();

router.get('/', function (req, res) {
    query.getAllPersons(req, res);
});

//passport
router.post('/login', passport.authenticate('local-login'),function(req,res){
    query.loginUser(req, res);
});

//This router handles a post register request to uri
router.post('/register', function (req, res) {
    query.registerUser(req, res);
});

router.post('/checkPass', passport.authenticate('local-login'),function(req,res){
    res.send("Old password OK!!");
});

//Handle UPDATE request for /users context
router.put('/changePass', function (req, res) {
    query.changePass(req, res);
});

//Handle POST request for /users context
router.post('/', function (req, res) {
    query.saveNewPerson(req, res);

});

//Handle UPDATE request for /users context
router.put('/', function (req, res) {
    query.updatePerson(req, res);
});

//Handle DELETE request for /users context
router.delete('/', function (req, res) {
    query.deletePerson(req, res);
});

router.get('/checkClass', function (req, res) {
    query.checkClass(req, res);
});


module.exports = router;